//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Bryan Winmill on 9/7/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let scrollView = UIScrollView(frame: self.view.bounds)
        self.view.addSubview(scrollView)
        scrollView.isPagingEnabled = true;
        scrollView.contentSize = CGSize(width: self.view.frame.size.width * 3 , height: self.view.frame.size.height * 3)
        
        let view1 = UIView(frame:CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view1.backgroundColor = UIColor.red
        scrollView.addSubview(view1)
        
        let view2 = UIView(frame:CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view2.backgroundColor = UIColor.blue
        scrollView.addSubview(view2)
        
        let view3 = UIView(frame:CGRect(x: 0, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view3.backgroundColor = UIColor.green
        scrollView.addSubview(view3)
        
        let view4 = UIView(frame:CGRect(x: self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view4.backgroundColor = UIColor.purple
        scrollView.addSubview(view4)
        
        let view5 = UIView(frame:CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view5.backgroundColor = UIColor.black
        scrollView.addSubview(view5)
        
        let view6 = UIView(frame:CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view6.backgroundColor = UIColor.cyan
        scrollView.addSubview(view6)
        
        let view7 = UIView(frame:CGRect(x: self.view.bounds.size.width * 2, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view7.backgroundColor = UIColor.brown
        scrollView.addSubview(view7)
        
        let view8 = UIView(frame:CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view8.backgroundColor = UIColor.gray
        scrollView.addSubview(view8)
        
        let view9 = UIView(frame:CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view9.backgroundColor = UIColor.yellow
        scrollView.addSubview(view9)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

